package com.pragma.user.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pragma.user.entity.User;
import com.pragma.user.service.UserService;


/**
 * Interface que controla el usuario, brinda las opciones de CRUD
 * @author Carlos Lopez
 *
 */

@RestController
@RequestMapping (value = "/users")
public class UserController {

	@Autowired
	private UserService userService;
	
	@GetMapping
	public ResponseEntity<List<User>> listUsers() {
		
		List<User> users = userService.listAllUser();
		if (users.isEmpty()) {
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok(users);
	}
	
	@GetMapping(value = "/{id}")
    public ResponseEntity<User> getProduct(@PathVariable("id") Long id) {
        User user =  userService.getUser(id);
        if (null==user){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(user);
    }
	
	@PostMapping
    public ResponseEntity<User> createUser(@Validated @RequestBody User user, BindingResult result){
        
        User userCreate =  userService.createUser(user);
        return ResponseEntity.status(HttpStatus.CREATED).body(userCreate);
    }
	
	@PutMapping(value = "/{id}")
    public ResponseEntity<User> updateUser(@PathVariable("id") Long id, @RequestBody User user){
        user.setId(id);
        User userDB =  userService.updateUser(user);
        if (userDB == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(userDB);
    }
	
	@DeleteMapping(value = "/{id}")
    public ResponseEntity<User> deleteUser(@PathVariable("id") Long id){
		User userDelete = userService.deleteUser(id);
        if (userDelete == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.OK).body(userDelete);
    }
	
}
