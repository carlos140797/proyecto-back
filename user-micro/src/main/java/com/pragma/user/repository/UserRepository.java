package com.pragma.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pragma.user.entity.User;


/**
 * 
 * @author Carlos Lopez
 *
 */
public interface UserRepository extends JpaRepository<User, Long> {



	public List<User> findByDocument(int document);
	
}