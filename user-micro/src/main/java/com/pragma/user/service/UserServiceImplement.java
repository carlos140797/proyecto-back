package com.pragma.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pragma.user.entity.User;
import com.pragma.user.repository.UserRepository;


@Service
public class UserServiceImplement implements UserService{
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public List<User> listAllUser() {
		
		return userRepository.findAll();
	}

	@Override
	public User getUser(Long id) {

		return userRepository.findById(id).orElse(null);
	}

	@Override
	public User createUser(User user) {

		user.setStatus("Activo");
		return userRepository.save(user);
	}

	@Override
	public User updateUser(User user) {
		User userdb = getUser(user.getId());
		if (userdb == null) {
			return null;
		}
		userdb.setAge(user.getAge());
		userdb.setCountry(user.getCountry());
		userdb.setDocument(user.getDocument());
		userdb.setDocumentType(user.getDocumentType());
		userdb.setLastName(user.getLastName());
		userdb.setName(user.getName());
		
		return userRepository.save(userdb);
		
	}

	@Override
	public User deleteUser(Long id) {
		User userdb = getUser(id);
		if (userdb == null) {
			return null;
		}
		userdb.setStatus("Eliminado");
		return userRepository.save(userdb);
	}

	@Override
	public List<User> findByDocument(int document) {
		
		return userRepository.findByDocument(document);
	}

}
