package com.pragma.user.service;

import java.util.List;

import com.pragma.user.entity.User;


public interface UserService {

	public List<User> listAllUser();
	public User getUser(Long id);
	
	public User createUser(User user);
    public User updateUser(User user);
    public User deleteUser(Long id);
    public List<User> findByDocument(int document);
}
