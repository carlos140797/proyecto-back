package com.pragma.user.entity;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data @AllArgsConstructor @NoArgsConstructor @Builder
@Table(name = "users")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "name", length = 20)
	private String name;
	
	@Column(name = "last_name", length = 20)
	private String lastName;
	
	@Column(name = "country", length = 20)
	private String country;
	
	@Column(name = "documentType", length = 20)
	private String documentType;
	
	
	private int document;
	
	@Column(name = "age")
	private int age;
	
	private String status;
	
}
