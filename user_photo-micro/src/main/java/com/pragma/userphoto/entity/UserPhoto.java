package com.pragma.userphoto.entity;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data @AllArgsConstructor @NoArgsConstructor @Builder
@Table(name = "user_photo")
public class UserPhoto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_user")
	private String idUser;
	
	@Column(name = "id_photo", length = 20)
	private String idPhoto;
		
}
