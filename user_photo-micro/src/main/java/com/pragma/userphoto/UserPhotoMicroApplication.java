package com.pragma.userphoto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class UserPhotoMicroApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserPhotoMicroApplication.class, args);
	}

}
