package com.pragma.userphoto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pragma.userphoto.entity.UserPhoto;
import com.pragma.userphoto.service.UserPhotoService;


/**
 * Interface que controla el usuario, brinda las opciones de CRUD
 * @author Carlos Lopez
 *
 */

@RestController
@RequestMapping (value = "/users")
public class UserPhotoController {

	@Autowired
	private UserPhotoService userService;
	
	@GetMapping
	public ResponseEntity<List<UserPhoto>> listUsers() {
		
		List<UserPhoto> users = userService.listAllUser();
		if (users.isEmpty()) {
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok(users);
	}
	
	@GetMapping(value = "/{id}")
    public ResponseEntity<UserPhoto> getProduct(@PathVariable("id") Long id) {
        UserPhoto user =  userService.getUser(id);
        if (null==user){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(user);
    }
	
	@PostMapping
    public ResponseEntity<UserPhoto> createUser(@Validated @RequestBody UserPhoto user, BindingResult result){
        
        UserPhoto userCreate =  userService.createUser(user);
        return ResponseEntity.status(HttpStatus.CREATED).body(userCreate);
    }
	
	@PutMapping(value = "/{id}")
    public ResponseEntity<UserPhoto> updateUser(@PathVariable("id") String id, @RequestBody UserPhoto user){
        user.setIdUser(id);
        UserPhoto userDB =  userService.updateUser(user);
        if (userDB == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(userDB);
    }
	
	@DeleteMapping(value = "/{id}")
    public ResponseEntity<UserPhoto> deleteUser(@PathVariable("id") Long id){
		UserPhoto userDelete = userService.deleteUser(id);
        if (userDelete == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(HttpStatus.OK).body(userDelete);
    }
	
}
