package com.pragma.userphoto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pragma.userphoto.entity.UserPhoto;


/**
 * 
 * @author Carlos Lopez
 *
 */
public interface UserPhotoRepository extends JpaRepository<UserPhoto, Long> {



	public List<UserPhoto> findByDocument(int document);
	
}