package com.pragma.userphoto.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pragma.userphoto.entity.UserPhoto;
import com.pragma.userphoto.repository.UserPhotoRepository;


@Service
public class UserPhotoServiceImplement implements UserPhotoService{
	
	@Autowired
	private UserPhotoRepository userRepository;

	@Override
	public List<UserPhoto> listAllUser() {
		
		return userRepository.findAll();
	}

	@Override
	public UserPhoto getUser(Long id) {

		return userRepository.findById(id).orElse(null);
	}

	@Override
	public UserPhoto createUser(UserPhoto user) {

		
		return userRepository.save(user); 
	}

	@Override
	public UserPhoto updateUser(UserPhoto user) {
		
		
		return userRepository.save(user);
		
	}

	@Override
	public UserPhoto deleteUser(Long id) {
		UserPhoto userdb = getUser(id);
		
		return userRepository.save(userdb);
	}

	@Override
	public List<UserPhoto> findByDocument(int document) {
		
		return userRepository.findByDocument(document);
	}

}
