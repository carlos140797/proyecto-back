package com.pragma.userphoto.service;

import java.util.List;

import com.pragma.userphoto.entity.UserPhoto;


public interface UserPhotoService {

	public List<UserPhoto> listAllUser();
	public UserPhoto getUser(Long id);
	
	public UserPhoto createUser(UserPhoto user);
    public UserPhoto updateUser(UserPhoto user);
    public UserPhoto deleteUser(Long id);
    public List<UserPhoto> findByDocument(int document);
}
