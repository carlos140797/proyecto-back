package com.pragma.photo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.pragma.photo.entity.Photo;

@Repository
public interface PhotoRepository extends MongoRepository<Photo, String>{

}
