package com.pragma.photo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class PhotoMicroApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhotoMicroApplication.class, args);
	}

}
