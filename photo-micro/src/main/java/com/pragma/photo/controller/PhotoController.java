package com.pragma.photo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pragma.photo.entity.Photo;
import com.pragma.photo.service.PhotoService;

@RestController
@RequestMapping(path = "photo")
public class PhotoController {
	
	@Autowired
	PhotoService photoService;
	
	
	@GetMapping
	public ResponseEntity<List<Photo>> listPhotos() {
		
		List<Photo> photos = photoService.listAllPhotos();
		if (photos.isEmpty()) {
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok(photos);
	}
	
	@GetMapping(value = "/{id}")
    public ResponseEntity<Photo> getPhoto(@PathVariable("id") String id) {
		Photo photo =  photoService.getPhoto(id);
        if (null==photo){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(photo);
    }
	
	@PostMapping
    public ResponseEntity<Photo> createPhoto(@Validated @RequestBody Photo photo, BindingResult result){
        
		Photo photoCreate =  photoService.createPhoto(photo);
        return ResponseEntity.status(HttpStatus.CREATED).body(photoCreate);
    }
	
	@PutMapping(value = "/{id}")
    public ResponseEntity<Photo> updatePhoto(@PathVariable("id") String id, @RequestBody Photo photo){
		photo.setId(id);
        Photo photoDB =  photoService.updatePhoto(photo);
        if (photoDB == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(photoDB);
    }
	
	@DeleteMapping(value = "/{id}")
    public ResponseEntity<Photo> deletePhoto(@PathVariable("id") String id){
		Photo photoDelete = photoService.deletePhoto(id);
        if (photoDelete == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(photoDelete);
    }
	

}
