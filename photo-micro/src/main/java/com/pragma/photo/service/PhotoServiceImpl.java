package com.pragma.photo.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pragma.photo.entity.Photo;
import com.pragma.photo.repository.PhotoRepository;


@Service
public class PhotoServiceImpl implements PhotoService {

	@Autowired
	private PhotoRepository photoRepository;	

	@Override
	public List<Photo> listAllPhotos() {
		System.out.println("null");
		if (photoRepository.findAll().isEmpty()) {
			
			return null;
		}
		return photoRepository.findAll();
	}

	@Override
	public Photo getPhoto(String id) {
		return photoRepository.findById(id).orElse(null);
	}

	@Override
	public Photo createPhoto(Photo photo) {
		return photoRepository.save(photo);
	}

	@Override
	public Photo updatePhoto(Photo photo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Photo deletePhoto(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	

}




