package com.pragma.photo.service;

import java.util.List;

import com.pragma.photo.entity.Photo;



public interface PhotoService{

	public List<Photo> listAllPhotos();
	public Photo getPhoto(String id);
	
	public Photo createPhoto(Photo user);
    public Photo updatePhoto(Photo user);
    public Photo deletePhoto(String id);
}
